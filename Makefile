TARGET=fpages
PACKAGES=core database log parser

.PHONY: all build run

all: build run

build:
	@go mod tidy
	@go mod vendor
	@go build -o ./build/$(TARGET) -buildvcs=false

run:
	@sudo ../go/bin/hydrogen protect ./build/$(TARGET) -f ceg
	@clear && chmod +x ./build/$(TARGET) && sudo ./build/$(TARGET)
