#!/bin/bash
#Install and update machine & fpages
# shellcheck disable=SC2002
# shellcheck disable=SC2181
# shellcheck disable=SC2006

if [ $# -ne 1 ]; then
  echo "Could not find domain. specify it via parameter e.g fpages.sh domain.com"
  exit 1
fi

domain=$1

echo "Stopping Services that may conflict"
service apache2 stop
service nginx stop

getSSlCert() {
  echo "fpages ssl certificates generation in process"

  certbot certonly --expand --manual --agree-tos --manual-public-ip-logging-ok --force-renewal \
    --register-unsafely-without-email --domain "${domain}" --domain "*.${domain}" --preferred-challenges dns

  # certbot certificates

  if [ $? -ne 0 ]; then
    echo "SSl failed for domain $domain"
    exit 1
  fi
  DefaultSSLDir="/etc/letsencrypt/archive"

  if [ ! -d "$DefaultSSLDir" ]; then
    echo "Cannot Find Default SSL Directory /etc/letsencrypt/archive"
    exit 1
  fi

  certFile=$(find /etc/letsencrypt/archive -type f -printf '%T@ %p\n' | sort -n | grep "cert" | tail -1 | cut -f2- -d" ")

  privkeyFile=$(find /etc/letsencrypt/archive -type f -printf '%T@ %p\n' | sort -n | grep "privkey" | tail -1 | cut -f2- -d" ")

  mkdir -p ./config/certificate/"$domain" &&
    cp "$certFile" ./config/certificate/"$domain/$domain".crt &&
    cp "$privkeyFile" ./config/certificate/"$domain/$domain".key
}

installGolang() {
  GO_VERSION=$(curl -s https://go.dev/dl/?mode=json | jq -r '.[0].version')
  if ! go version | grep -q "${GO_VERSION}"; then
    echo "Downloading and installing Go ..."
    # Remove existing Go installation if it exists
    if [ -d "/usr/local/go" ]; then
      sudo rm -rf /usr/local/go
    fi
    curl -LO https://go.dev/dl/"${GO_VERSION}".linux-amd64.tar.gz &&
      sudo tar -C /usr/local -xzf "${GO_VERSION}".linux-amd64.tar.gz && ln -sf /usr/local/go/bin/go /usr/bin/go &&
      sudo rm "${GO_VERSION}".linux-amd64.tar.gz && ln -sf /usr/local/go/bin/go /usr/bin/go &&
      go install golang.org/x/tools/...@latest && go install github.com/TryZeroOne/hydrogen@latest
  else
    echo "Go version ${GO_VERSION} already installed, skipping installation."
  fi
}

installNodejs() {
  if which node >/dev/null; then
    echo "nodejs is installed, skipping instalation ..."
  else
    # Remove existing Go installation if it exists
    sudo rm -rf /usr/local/bin/npm && sudo rm -rf /usr/local/share/man/man1/node* && sudo rm -rf /usr/local/lib/dtrace/node.d &&
      sudo rm -rf ~/.npm && sudo rm -rf ~/.node-gyp && sudo rm -rf /opt/local/bin/node && sudo rm -rf opt/local/include/node &&
      sudo rm -rf /opt/local/lib/node_modules && sudo rm -rf /usr/local/lib/node* && sudo rm -rf /usr/local/include/node* &&
      sudo rm -rf /usr/local/bin/node* && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash &&
      export NVM_DIR="$HOME/.nvm" && [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" &&
      nvm install --lts && npm install pm2@latest -g && pm2 completion install && pm2 update && echo "Nodejs has been installed."
  fi
}

os=$(cat /etc/os-release | awk -F '=' '/^NAME/{print $2}' | awk '{print $1}' | tr -d '"')
if [ "$os" == "Ubuntu" ]; then
  sudo apt-get install jq libc6 make curl snapd build-essential certbot upx &&
    snap install core && snap refresh core && sudo sed -i 's/#DNSStubListener=yes/DNSStubListener=no/' /etc/systemd/resolved.conf &&
    sudo systemctl restart systemd-resolved && getSSlCert && installGolang && installNodejs && make
else
  echo "system is $os. fpages cannot run on this device please try again on a linux ubuntu device."
fi
